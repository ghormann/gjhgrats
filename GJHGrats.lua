GJHGrats = LibStub("AceAddon-3.0"):NewAddon("GJHGrats", "AceComm-3.0");
--
-- Variables
-- ------------------------
-- status: Normally -1, gets set to zero to send a message. Messages are sent 2 seconds after notification to prevent multiple messages for related achievements).
-- timeGuild: Time last Guild update
-- timeLeader: Time last Leader update
-- playerName: Name of player (self)
-- channel: Name of Channel to send message ("Guild")
-- lastPlayer: Last person to have an achievement.
--


--
-- Notification message in default window (Normally for debugging.)
--
function trace(msg)
	DEFAULT_CHAT_FRAME:AddMessage(msg);
end



function GJHGrats.OnLoad(frame)
	frame:RegisterEvent("ADDON_LOADED");
	frame:RegisterEvent("CHAT_MSG_GUILD_ACHIEVEMENT");
	frame:RegisterEvent("GUILD_ROSTER_UPDATE");
	frame:SetScript("OnUpdate", GJHGrats.onUpdate);
	
	SLASH_GJHGrats1 = "/GJHGrats";
	SLASH_GJHGrats2 = "/GJH";
	SlashCmdList["GJHGrats"] = GJHGrats.CLI;
	SlashCmdList["GJH"] = GJHGrats.CLI;
end

--
-- If GJHGrats > 0 then there is a message that needs sent
-- We wait 2 seconds to avoid multiple messages.
--
-- NOTE: On Update doesn't fire if tabbed out!
-- 
function GJHGrats.onUpdate(self, elapsed)
	local timeNow = time();
	GJHGrats.lastUpdate = timeNow;

	if (GJHGrats.status >= 0) then
		GJHGrats.status = GJHGrats.status + elapsed;
      	if (GJHGrats.status >= 2) then
			GJHGrats.sendMessage(); -- Really send the message
			GJHGrats.status = -1;
		end;
	end;

	-- refresh guild list once every 60 seconds to check for new guild members and level changes.
	if (GJHGrats.timeGuild + 60 < timeNow) then 
		GJHGrats.timeGuild = timeNow;
		GJHGrats.refreshGuild()
	end;
	-- Confirm who the leader is in a multi-user environment
	if (GJHGrats.timeLeader + 7 < timeNow) then 
		GJHGrats.timeLeader = timeNow;
		GJHGrats:RequestLeader();
	end;

end;

--
-- Schedules sending out a guild message.
-- If the message is for my achievement, and I'm the leader
-- ask the backup
--
function GJHGrats.schedule()
	if (GJHGrats.isLeader) then
		if (GJHGrats_Options.lastPlayer == GJHGrats_Options.playerName) then
			GJHGrats:LeaderGrats();
			trace("GJHGrats: Not gratting self!");
		else
			GJHGrats.status = 0;
		end
	end
end;

--
-- Request that the Guild Information be refreshed.
--
function GJHGrats.refreshGuild()
	if IsInGuild() then
		if GetGuildRosterShowOffline() == nil then
			SetGuildRosterShowOffline(1)
		end
		GuildRoster()	
	end
end

--
-- Callback function for refreshGuild()
-- Identifies any guild members that don't have a real name
-- Also tracks level changes
--
function GJHGrats.handleGuildInfo()
	local timeNow = time();
	GJHGrats.timeGuild =timeNow;

	local players = GJHGrats:getPlayers();
	local missing = "Missing:"
	for i = 1, GetNumGuildMembers() do
		local name, rank, rankIndex, level, class, zone, note, officernote, online, status, classFileName, achievementPoints, achievementRank, isMobile, canSoR = GetGuildRosterInfo(i);

		--
		-- If we've never seen the player before, create a record
		--
		if (players[name] == nil) then
			trace("New record for " .. name);
			players[name] = {};
		end
		local player = players[name];

		if (online ~= nil) then
			if (player.realname == nil) then
				missing = missing .. name .. ", "
				 GJHGrats:SendMissing(name);  -- Send to see if anybody else has it.
			end
			if (player.level ~= nil) then
				if (level > player.level) then
					local niceName = GJHGrats.convertName(name);
					local isMe = false;
					if (name == GJHGrats_Options.playerName ) then
						msg = "Ding! - " .. level;
						isMe = true;
						GJHGrats.sendGuildMessage(msg)
					end
					if (GJHGrats.isLeader and not isMe) then 
						local msg = "Congrats " .. niceName .. " on reaching level  " .. level .. GJHGrats:getFromLine(name);
						GJHGrats.sendGuildMessage(msg)
					end
				end
			end
		end

		player.level = level;
	end;
	-- If there are online members that don't have a real name, print them
	-- They can be set with /gjhGrats {toonname} {realname}
	if (missing:len() > 8) then
		trace(missing);
	end

end

--
-- Returns a hash of names=>Info for the current guild
--
function GJHGrats:getPlayers()
	if (GJHGrats.guild == "NONE" ) then
		if IsInGuild() then
			local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
			GJHGrats.guild = guildName;
			--trace("GJHGrats: Guild is " .. GJHGrats.guild);
		end
	end
	if (GJHGrats_Options.players[GJHGrats.guild] == nil) then
		GJHGrats_Options.players[GJHGrats.guild] = {};
	end

	return GJHGrats_Options.players[GJHGrats.guild];
end

function GJHGrats.OnEvent(self,event,...)
	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11 = ...;
	if(event == "ADDON_LOADED") then
		if arg1 == "GJHGrats" then
			GJHGrats.guild = "NONE";
			GJHGrats.OriginalChatMsgHandler = ChatFrame_MessageEventHandler;
			ChatFrame_MessageEventHandler = GJHGrats.ChatMsgHandler;
			
			if (GJHGrats_Options == nil) then
				GJHGrats_Options = {};
			end

			if (GJHGrats_Options.players == nil) then
				GJHGrats_Options.players = {};
			end

			if GJHGrats_Options.active == nil then GJHGrats_Options.active = true; end
			GJHGrats_Options.playerName = GetUnitName("player", false)  .. "-" .. GetRealmName();
			GJHGrats_Options.channel = "GUILD";
			GJHGrats_Options.lastPlayer = "Neep";
			GJHGrats_Options.active = false; -- Not really used...
			
			GJHGrats.timeSinceSpam = 0;
			GJHGrats.timeGuild = 0;
			GJHGrats.lastUpdate = time();
			GJHGrats.isLeader = false;
			GJHGrats.timeLeader = time();
                  	GJHGrats.status = -1;   -- Nothing to send yet.
			GJHGrats:ConfigComm();
			GJHGrats.refreshGuild();
			trace("GJHGrats is loaded");
		end
	end
	if (event == "CHAT_MSG_GUILD_ACHIEVEMENT") then
		GJHGrats_Options.lastPlayer = arg2;
		GJHGrats.schedule();
	end
	if (event == "GUILD_ROSTER_UPDATE") then
		GJHGrats.handleGuildInfo();
	end
	
end

--
-- 95% sure this is dead code.  Notifications now come via CHAT_MSG_GUILD_ACHIEVEMENT rather than
-- Guild Chat Messages
--
function GJHGrats.ChatMsgHandler(self, event, ...)
	GJHGrats.OriginalChatMsgHandler(self, event, ...);  -- Send chat message to default handler
	
	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11 = ...;
	if event=="CHAT_MSG_GUILD" then
		if (string.find(arg1, "has earned the achievement") == nil) then
			return;
		end;
		GJHGrats.schedule(); -- Spam my friends!
	end
end

--
-- Comandline interface
--
function GJHGrats.CLI(options)
	if (options == nil) then
		trace("Usage /gjhgrats username realname");
	elseif (options == "send") then
		GJHGrats:SendAll();
	elseif (options == "who") then
		GJHGrats:who();
	elseif (options == "god") then
		GJHGrats_Options.isGod = not GJHGrats_Options.isGod;
		trace("GJHGrats: God mode is " .. tostring(GJHGrats_Options.isGod));
	elseif (options == "take") then
		trace("GJHGrats: Taking Leader");
		GJHGrats:SetAsLeader();
	elseif (options == "leader") then
		local backup = GJHGrats:getBackup();
		trace("Leader is " .. GJHGrats.Leader .. ", Backup: " .. backup);
	--elseif (options == "migrate") then
--		GJHGrats:migrate();
	elseif (options == "") then
		trace("Usage /gjhgrats leader  == Shows current leader");
		trace("Usage /gjhgrats who  == See who has it installed and what version");
		trace("Usage /gjhgrats send  == Broadcast all to update others.");
		trace("Usage /gjhgrats username realname == Set Real name for User (Case Sensitive) ");
	else
		--trace("CLI");
		local username, realname = strsplit(" ", options, 2)
		GJHGrats.saveUser(username, realname);
	end	
end

--
-- Saves a pairing of username and realname.  
-- Also broadcast to other users
--
function GJHGrats.saveUser(username, realname)
	username = username .. "-" .. GetRealmName(); -- Wow 5.5.2 made Realm part of name.
	local players = GJHGrats:getPlayers();
	local player = players[username];
	if (player == nil ) then
		trace("Invalid user " .. username);
	else
		player.realname = realname;
		trace("Saved");
		GJHGrats:SendName(username,realname);
	end 
end

--
-- Returns a randomly selected message
--
function GJHGrats.getMessage()
	local i = random(1, #GJHGrats_Messages);
	return GJHGrats_Messages[i];
end

function GJHGrats.migrate()
	trace("Migrating");
	local players = GJHGrats:getPlayers();
	local name;
	local v;
	for name, v in pairs(players) do
		local pos = string.find(name, "-");
		if (pos ~= nil) then
			local other = string.sub(name, 1, pos-1);
			other = GJHGrats.convertName(other);
			trace("Testing " .. other)
			players[name].realname = other;
		end
	end
end

--
-- Sends a message on the Guild Channel
--
function GJHGrats.sendGuildMessage(msg)
	local _,_,chatType = string.find(GJHGrats_Options.channel, "^(%w+)");
	local _,_,channel = string.find(GJHGrats_Options.channel, "^%w+ (%d)$");
	channel = tonumber(channel);
	SendChatMessage(msg ,chatType ,nil ,channel);
end

--
-- Attempts to convert a toon name to a real name.
-- If a real name can't be found, return the toon name
--
function GJHGrats.convertName(name)
	local players = GJHGrats:getPlayers();
	local player = players[name];
	if (player ~= nil) then
		if (player.realname ~= nil) then
			name = player.realname
		end
	end

	return name
end

--
-- This function is called to send a Congrats message.
-- Should only be called by leader (or backup if leader had achievement.)
--
function GJHGrats.sendMessage()
	local timeNow = time();
	if (GJHGrats.timeSinceSpam + 2 > timeNow) then return end;  -- DOn't keep spamming.
	local name = GJHGrats.convertName(GJHGrats_Options.lastPlayer);

	local msg = string.gsub(GJHGrats.getMessage(), "%%t", name); -- Substitude in name
	GJHGrats.sendGuildMessage(msg .. GJHGrats:getFromLine(GJHGrats_Options.lastPlayer))
	trace(GJHGrats_Options.lastPlayer);
	GJHGrats.timeSinceSpam = timeNow;
end

