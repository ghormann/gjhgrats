﻿## Interface: 50400
## Title: GJHGrats
## Author: Greg Hormann
## Notes: 
## eMail: ghormann@gmail.com
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: GJHGrats_Options
## Version: 0.8

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@


GJHGrats.lua
GJHGrats.xml
GJHGratsComm.lua
messages.lua
