
local MSG_PREFIX = "GJHGRATS";
local MSG_DIST = "GUILD";
local MSG_ASK_LEADER="LEADER?";

--
-- Initial configuration. Should be called in Addon Load
--
function GJHGrats:ConfigComm()
	trace("GJHGrats:ConfigComm");
	GJHGrats.backups = {};
	GJHGrats.Leader = "Nobody";
	GJHGrats.leaderCnt = 0;
	GJHGrats.versionCnt = 99999;
	GJHGrats.version=6;
	GJHGrats.upgradeUrl="https://bitbucket.org/ghormann/gjhgrats/downloads";
	GJHGrats:RegisterComm(MSG_PREFIX, "OnCommReceive");
	GJHGrats:RequestLeader();
end

--
-- Called every 7 seconds to figure out who is the leader.
-- The undocumented God Mode option allows the user to always 
-- become leader.
--
function GJHGrats:RequestLeader()
	if IsInGuild() then
		if (GJHGrats_Options.isGod and not GJHGrats.isLeader ) then
			trace("God mode: Taking Leader");
			GJHGrats:SetAsLeader();
		end

		local now = time();
		GJHGrats.leaderCnt = GJHGrats.leaderCnt + 1;
		if (GJHGrats.leaderCnt > 2) then
			trace("GJHGrats: No Leader after 3 request.  I'm Leader");
			GJHGrats:SetAsLeader();
		end

		local msg = "";
		if GJHGrats.isLeader then
			msg = "LEADER:" .. GJHGrats_Options.playerName .. ":" .. GJHGrats.version;
			GJHGrats.leaderCnt = 0;
		else
			msg = MSG_ASK_LEADER .. GJHGrats_Options.playerName .. "|" .. GJHGrats.version;
		end
		GJHGrats:SendCommMessage(MSG_PREFIX,msg,MSG_DIST);

		-- Check Version
		GJHGrats.versionCnt = GJHGrats.versionCnt + 1;
		if (GJHGrats.versionCnt > 500) then
			GJHGrats.versionCnt = 0
			GJHGrats:RequestVersion();
		end
		
	end
end

function GJHGrats:RequestVersion()
	local msg = "VERSION:" .. GJHGrats.version .. ":" .. GJHGrats.upgradeUrl
	GJHGrats:SendCommMessage(MSG_PREFIX,msg,MSG_DIST);
end

--
-- Notify others that I don't have a real name for specified users
-- 
function GJHGrats:SendMissing(name)
	local msg = "MISSING:" .. name;
	GJHGrats:SendCommMessage(MSG_PREFIX,msg,MSG_DIST);
end

function GJHGrats:getBackup(avoid)
	local name, values;
	local now = time();
	for name,values in pairs(GJHGrats.backups) do
		if (avoid ~= name and values.lastTime + 20 > now and values.isLeader == false) then
			return name
		end;
	end
	return "";

end
function GJHGrats:who()
	local name, values;
	local now = time();

	for name,values in pairs(GJHGrats.backups) do
		if (values.lastTime + 20 > now ) then
			local msg = name .. " version: " .. values.version;
			if (values.isLeader) then
				msg = msg .. ", Leader";
			end
			trace(msg);
		end;
	end
	return "";

end

--
-- Send notification that I'm the leader and I have a grats
--
function GJHGrats:LeaderGrats()
	local backup = GJHGrats:getBackup(GJHGrats_Options.playerName);
	if (backup ~= "") then
		GJHGrats:SendCommMessage(MSG_PREFIX,GJHGrats_Options.playerName,"WHISPER",backup);
	end
end

--
-- Take leadership and notify all others
--
function GJHGrats:SetAsLeader()
	GJHGrats.isLeader = true;
	GJHGrats.leaderCnt = 0;
	GJHGrats:RequestLeader() -- will send leader
end

--
-- Send announcement that a toon and a new real name
--
function GJHGrats:SendName(who,real)
	trace("Sending " .. who .. " is really " .. real);
	local msg = "SETNAME:" .. who .. "|" .. real;
	GJHGrats:SendCommMessage(MSG_PREFIX,msg,MSG_DIST);
end

--
-- Send all the real names I know
--
function GJHGrats:SendAll()
	local players = GJHGrats:getPlayers();
	local name,data;
	trace("Sending all");
	for name,data in pairs(players) do
		if (data.realname ~= nil) then
			local real = data.realname;
			trace("Name: " .. name .. " is " .. real);
			GJHGrats:SendName(name,real);
		end
	end;
end;

--
-- Get Form Line
--
function GJHGrats:getFromLine(forUser)
	local msg = " --- On behalf of " .. GJHGrats.convertName(GJHGrats_Options.playerName);
	local now = time();
	local name,data;

	for name,data in pairs(GJHGrats.backups) do
		if (data.lastTime + 20 > now and forUser ~= name and name ~= GJHGrats_Options.playerName) then
			msg  = msg .. ", " .. GJHGrats.convertName(name)
		end;
	end
	msg  = msg .. " (via GJHGrats)"
	return msg;
end;

function saveBackup(name,version,isLeader)
	if (version == nil) then
		version = "0";
	end
	if (GJHGrats.backups[name] == nil) then
		GJHGrats.backups[name]= {};
	end
	GJHGrats.backups[name].lastTime = time();
	GJHGrats.backups[name].version = version;
	GJHGrats.backups[name].isLeader = isLeader;
end

--
-- Handler for messages comming in
--
function GJHGrats:OnCommReceive(prefix, Msgs, distribution, target)
	local now = time();
	if (GJHGrats.lastUpdate + 4 < now and GJHGrats.isLeader) then
		local backup = GJHGrats:getBackup(GJHGrats_Options.playerName);
		if (backup ~= "") then
			trace("Giving Leader away to " .. backup .. ". OnUpdate is not firing");
			GJHGrats.isLeader = false;
			msg = "LEADER:" .. backup;
			GJHGrats:SendCommMessage(MSG_PREFIX,msg,MSG_DIST);
		end
	end

	if (distribution == "WHISPER") then                   -- Leader asked me to send it.....
		GJHGrats_Options.lastPlayer = Msgs;
		GJHGrats.sendMessage();
	elseif string.sub(Msgs,1,string.len(MSG_ASK_LEADER) ) == MSG_ASK_LEADER then
		local body = string.sub(Msgs,string.len(MSG_ASK_LEADER)+1, string.len(Msgs));
		local from, version = strsplit("|", body, 2)    
		if (from == nil ) then
			from = body
		end
		if (version == nil) then
			version = "0";
		end

		saveBackup(from,version,false);


		if (GJHGrats.isLeader) then
			GJHGrats:RequestLeader() -- will send leader
		end

	elseif string.sub(Msgs,1,8) == "SETNAME:" then   -- Handle being notified of a new real name
		local body = string.sub(Msgs,9, string.len(Msgs));
		local username, realname = strsplit("|", body, 2)    
		trace("REMOTE SET: " .. username .. " is " .. realname);
	        local players = GJHGrats:getPlayers();
        	local player = players[username];
        	if (player == nil ) then
                	players[username] = {};
        		player = players[username];
		end
               	player.realname = realname;


	elseif string.sub(Msgs,1,8) == "VERSION:" then  -- Asking about Version
		local body = string.sub(Msgs,9, string.len(Msgs));
		local versionstr, url = strsplit(":", body, 2)    
		local version = tonumber(versionstr);

		if (version > GJHGrats.version) then
			trace("New version of GJHGrats available at: " + url);
		elseif (version < GJHGrats.version) then
			GJHGrats:RequestVersion();  -- Sends my version notifing others of upgrade.
		end

	elseif string.sub(Msgs,1,8) == "MISSING:" then  -- Handle request to fix a missing person
		local who = string.sub(Msgs,9, string.len(Msgs));
		local players = GJHGrats:getPlayers();
		if players[who] ~= nil then
			if (players[who].realname ~= nil) then
				GJHGrats:SendName(who,players[who].realname);
			end
		end

	elseif string.sub(Msgs,1,7) == "LEADER:" then   -- Handle request to assign new leader
		local body = string.sub(Msgs,8, string.len(Msgs));
		local newLeader, version = strsplit(":", body, 2)    
		if (newLeader == nil) then
			newLeader = body -- Old Format
		end
		GJHGrats.leaderCnt = 0;
		GJHGrats.Leader = newLeader;
		if (newLeader ~= GJHGrats_Options.playerName) then
			if GJHGrats.isLeader then
				GJHGrats.isLeader = false;
				trace("GJHGrats: I'm no longer the leader. New Leader: " .. newLeader);
			end;
		elseif (GJHGrats.isLeader == false) then
			GJHGrats.isLeader = true;
			trace("GJHGrats: I'm the new leader: " .. newLeader);
		end;
		saveBackup(newLeader,version,true);
	else
		trace("WARNING: Unknown message: " .. Msgs);
	end;

end

