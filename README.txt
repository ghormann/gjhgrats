A World of Warcraft addon that sends out messages via guild chat when guild members complete achievements.  

Has the capability to use the user's "real" first name if known.  ALso, communicates with others
in the guild running the addon to only send one message for the whole group.